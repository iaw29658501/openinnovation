<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Idea;
use App\Entity\IdeaStatus;
use App\Entity\Like;
use App\Entity\Note;
use App\Entity\Sector;
use App\Entity\Tag;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private UserPasswordEncoderInterface $encoder;
    protected $faker;
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder= $encoder;
        $this->faker= Factory::create();
    }

    public function load(ObjectManager $manager)
    {
        $sector_marketing = new Sector();
        $sector_marketing->setName('marketing');
        $manager->persist($sector_marketing);

        $sector_ing = new Sector();
        $sector_ing->setName('ingeniería');
        $manager->persist($sector_ing);

        $sector_tech = new Sector();
        $sector_tech->setName('tech');
        $manager->persist($sector_tech);

        $sector_contabilidad = new Sector();
        $sector_contabilidad->setName('contabilidad');
        $manager->persist($sector_contabilidad);
        $manager->flush();
        $sectors = [$sector_marketing,$sector_contabilidad,$sector_tech,$sector_ing];
        // TAGS
        $tag_eco = new Tag();
        $tag_eco->setName("eco");
        $manager->persist($tag_eco);

        $tag_mejoria = new Tag();
        $tag_mejoria->setName("mejoría");
        $manager->persist($tag_mejoria);

        $tag_bici = new Tag();
        $tag_bici->setName("bici");
        $manager->persist($tag_bici);

        $tagnames=["deportistas","salud","inversiones","expansion","clientes","ventas"];
        for($i=0;$i<15;$i++){
            $tagnames[] = $this->faker->text(12);
        }

        $tags = [$tag_eco,$tag_mejoria,$tag_bici];
        foreach ($tagnames as $tagname) {
            $$tagname = new Tag();
            $$tagname->setName($tagname);
            $manager->persist($$tagname);
            $tags[]=$$tagname;
        }
        // STATUS
        $status1 = new IdeaStatus();
        $status1->setName('Idea nueva');
        $status1->setNumber(1);
        $manager->persist($status1);

        $status2 = new IdeaStatus();
        $status2->setName('Madurando idea');
        $status2->setNumber(2);
        $manager->persist($status2);

        $status3 = new IdeaStatus();
        $status3->setName('Aprobada');
        $status3->setNumber(3);
        $manager->persist($status3);

        $status_maybe_later = new IdeaStatus();
        $status_maybe_later->setName('Talvez despues');
        $status_maybe_later->setColor('#f00');
        $status_maybe_later->setNumber(3);
        $manager->persist($status_maybe_later);

        $status4 = new IdeaStatus();
        $status4->setName('Implementando');
        $status4->setNumber(4);
        $manager->persist($status4);

        $status5 = new IdeaStatus();
        $status5->setName('Realizada');
        $status5->setNumber(5);
        $manager->persist($status5);

        $statuses=[$status1,$status2,$status3,$status4,$status5,$status_maybe_later];

        // USERS
        $admin = new User();
        $admin->setEmail('admin@localhost.com');
        $admin->setName('Admin');
        $admin->setPassword($this->encoder->encodePassword($admin,'admin'));
        $admin->setRoles(['ROLE_ADMIN']);
        $manager->persist($admin);

        $cauan = new User();
        $cauan->setEmail('cauan@localhost.com');
        $cauan->setName('Cauan');
        $cauan->setPassword($this->encoder->encodePassword($cauan,'user_pass'));
        $cauan->setRoles(['ROLE_USER']);
        $cauan->addSector($sector_tech);
        $manager->persist($cauan);

        $cauan = new User();
        $cauan->setEmail('viewme@localhost.com');
        $cauan->setName('EXViewer');
        $cauan->setPassword($this->encoder->encodePassword($cauan,'user_pass'));
        $cauan->setRoles(['ROLE_VIEWER']);
        $cauan->addSector($sector_tech);
        $manager->persist($cauan);


        $toni = new User();
        $toni->setEmail('toni@localhost.com');
        $toni->setName('Toni');
        $toni->setPassword($this->encoder->encodePassword($toni,'user_pass'));
        $toni->setRoles(['ROLE_ADMIN']);
        $toni->addSector($sector_tech);
        $manager->persist($toni);

        $mario = new User();
        $mario->setEmail('mario@localhost.com');
        $mario->setName('Mario');
        $mario->setPassword($this->encoder->encodePassword($mario,'user_pass'));
        $mario->setRoles(['ROLE_USER']);
        $mario->setLastRequest((new DateTime())->modify('- 5 days'));
        $mario->addSector($sector_marketing);
        $manager->persist($mario);

        $martacarrasco = new User();
        $martacarrasco->setEmail('martacarrasco@localhost.com');
        $martacarrasco->setName('Marta Carrasco');
        $martacarrasco->setPassword($this->encoder->encodePassword($martacarrasco,'user_pass'));
        $martacarrasco->setRoles(['ROLE_USER']);
        $martacarrasco->setLastRequest((new DateTime())->modify('- 5 days'));
        $martacarrasco->addSector($sector_marketing);
        $manager->persist($martacarrasco);

        $users=[$cauan,$toni,$mario,$martacarrasco,$admin];
        // IDEAS
        $idea = new Idea();
        $idea->setName('2 weeks ago idea');
        $idea->setTitle('Time machine ipsum dorem');
        $idea->setDescription('Est s asd, cesaris.Sunt dsds demitto emeritis, fatalis omniaes.Garnish one jar of chicken in four pounds of mayonnaise.Vision at the solar system was the hypnosis of definition, contacted to a futile astronaut.');
        $idea->addTag($tag_bici);
        $idea->setOwner($cauan);
        $idea->addSector($sector_tech);
        $idea->setStatus($status3);
        $idea->setAnonymous(true);
        $idea->setPublishedAt((new DateTime)->modify('-14 days'));
        $manager->persist($idea);
        for ($i=0;$i<50;$i++) {
            $idea = new Idea();
            $idea->setName($this->faker->text(60));
            $idea->setTitle($this->faker->text(120));
            $idea->setDescription($this->faker->text(300));
            $idea->setOwner($this->faker->randomElement($users));
            $idea->setStatus($this->faker->randomElement($statuses));
            $idea->setAnonymous($this->faker->boolean($chanceOfGettingTrue = 25));
            if ($i > 32) {
                $idea_date = $this->faker->dateTimeBetween('-1 day','now');
            } else if ($i > 15) {
                    $idea_date = $this->faker->dateTimeBetween('-1 week','now');
            } else {
                $idea_date = $this->faker->dateTimeBetween('-2 weeks');
            }
            $idea->setPublishedAt($idea_date);
            // SECTORS
            $idea_sectors=$this->faker->randomElements($sectors,2);
            foreach($idea_sectors as $sector){
                $idea->addSector($sector);
            }
            // TAGS
            $idea_tags=$this->faker->randomElements($tags,2);
            foreach($idea_tags as $tag){
                $idea->addTag($tag);
            }
            // COMMENTS
            $num_comments = $this->faker->numberBetween(0,count($users));
            $idea_comments_datetime_spacer = $idea_date->modify('+' . $this->faker->numberBetween(1,20) . ' minutes');
            for($j=0;$j<$num_comments;$j++) {
                $comment = new Comment();
                $comment->setUser($users[$this->faker->numberBetween(0,count($users)-1)]);
                $comment->setText($this->faker->text(60));
                $comment->setPublishedAt($idea_comments_datetime_spacer);
                $idea->addComment($comment);
                $manager->persist($comment);
                $idea_comments_datetime_spacer = $idea_date->modify('+'.$this->faker->numberBetween(5, 100) . ' minutes');
            }
            // LIKES
            $num_likes = $this->faker->numberBetween(3,5);
            $users_that_liked = $this->faker->randomElements($users,$num_likes);
            foreach($users_that_liked as $user) {
                $like = new Like();
                $like->setUser($user);
                $like->setDatetime($idea_date->modify('+'.'1'.' hours'));
                $idea->addLike($like);
                $manager->persist($like);
            }
            // NOTES
            $users_to_note = $this->faker->randomElements($users, $this->faker->numberBetween(2,count($users)-1 ));
            foreach ($users_to_note as $user) {
                if ($user !== $idea->getOwner()) {
                    $note = new Note();
                    $note->setUser($user);
                    $note->setFinancialNote($this->faker->numberBetween(4,10));
                    $note->setUrgencyNote($this->faker->numberBetween(4,10));
                    $idea->addNote($note);
                    $manager->persist($note);
                }
            }
            $manager->persist($idea);
        }


        $manager->flush();
    }
}
