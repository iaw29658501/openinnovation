<?php

namespace App\Security\Voter;

use App\Entity\Idea;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class IdeaVoter extends Voter
{
    /**
     * @var Security
     */
    private Security $security;

    /**
     * IdeaVoter constructor.
     * @param Security $security
     */
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['IDEA_EDIT', 'IDEA_VIEW','IDEA_COMMENT','IDEA_NOTE','IDEA_STATUS'])
            && $subject instanceof \App\Entity\Idea;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /** @var Idea $subject */
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }
        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'IDEA_EDIT':
                if ( $subject->getOwner() == $user || $this->security->isGranted('ROLE_IDEA_EDIT')){
                    return true;
                }
                break;
            case 'IDEA_VIEW':
                // logic to determine if the user can VIEW
                // return true or false
                if ( $this->security->isGranted('ROLE_IDEA_SHOW')){
                    return true;
                }
                break;
            case 'IDEA_NOTE':
                if ( $subject->getOwner() != $user || $this->security->isGranted('ROLE_NOTE_NEW')){
                    return true;
                }
                break;
            case 'IDEA_COMMENT':
                if ( $this->security->isGranted('IDEA_VIEW', $subject) && $this->security->isGranted('ROLE_COMMENT_NEW') ){
                    return true;
                }
                break;
            case 'IDEA_STATUS':
                if ( $this->security->isGranted('ROLE_IDEA_STATUS') ){
                    return true;
                }
                break;
        }

        return false;
    }
}
