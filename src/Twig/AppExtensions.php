<?php


namespace App\Twig;


use Carbon\Carbon;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
class AppExtensions extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('date_time_ago',[$this,'dateTimeAgo'])
        ];
    }
    public function dateTimeAgo($twigInput): string
    {
        Carbon::setLocale('es');
        return Carbon::parse($twigInput)->diffForHumans();
    }

}