<?php


namespace App\Service;


use App\Entity\File as FileEntity;
use App\Entity\Idea;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraints\File as FileValidator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UploadHelper
{
    private string $uploadPath;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;
    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;
    /**
     * @var Security
     */
    private Security $security;

    /**
     * UploadHelper constructor.
     * @param string $uploadPath
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     * @param Security $security
     */
    public function __construct(string $uploadPath, EntityManagerInterface $entityManager, ValidatorInterface $validator,Security $security)
    {
        $this->uploadPath = $uploadPath;
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->security = $security;
    }

    public function uploadIdeaFile(File $file, Idea $idea)
    {
        $destination=$this->uploadPath;
        $violations=$this->validator->validate(
            $file,
            new FileValidator([
                // TODO Check if your php.ini is well configured
                'maxSize' => '25M',
                'mimeTypes' => [
                    'application/pdf',
                    'application/x-pdf',
                    'image/jpeg',
                    'image/png',
                    // TODO: test against MACROS phishing
                    'application/msword',
                    'application/vnd.ms-excel',
                    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                    'text/plain',
                    'application/x-rar-compressed',
                    'application/zip'
                ],
                'mimeTypesMessage' => 'Formato de archivo inválido',
            ])
        );
        if ($violations->count() > 0 ) {
            throw new \Exception(sprintf('Validation error "%s"', $violations));
        }
        if ($file instanceof File ) {
            $originalFilename = $file->getClientOriginalName();
        } else {
            $originalFilename=$file->getFilename();
        }
        // remove file client extension suggestion
        $originalFilename = pathinfo( $originalFilename, PATHINFO_FILENAME);
        // this is needed to safely include the file name as part of the URL
        $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
        $frontendFilename = $safeFilename .'.'. $file->guessExtension();
        $newFilename = $safeFilename . '-' . uniqid('', true) . '.' . $file->guessExtension();
        // Save the user file
        try {
            $file->move(
                $destination,
                $newFilename
            );
        } catch (FileException $e) {
            // dump("file move exception");
            // ... TODO low: handle exception if something happens during file upload
        }

        $fileEntity = new FileEntity();
        $fileEntity->setName($newFilename);
        $fileEntity->setIdea($idea);
        $fileEntity->setOriginalName($frontendFilename);
        $idea->addFile($fileEntity);
        if ( ! $idea->getPublishedAt() ) { $idea->setPublishedAt(new DateTime()); }
        $idea->setLastUpdateAt(new \DateTime());
        $idea->setOwner($this->security->getUser());
        try {
            $this->entityManager->persist($fileEntity);
            $this->entityManager->persist($idea);
            $this->entityManager->flush();
        } catch (ORMException $e) {
        }
        return $newFilename;
    }
}
