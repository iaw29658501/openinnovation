<?php

namespace App\Repository;

use App\Entity\Comment;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comment::class);
    }

    /**
     * @param DateTime $first_datetime
     * @param DateTime|null $last_datetime
     * @return Comment[] Returns an array of Idea objects
     */
    public function publishedBetween(\DateTimeInterface $first_datetime, \DateTimeInterface $last_datetime = null): array
    {
        if ($last_datetime == null) {
            $last_datetime = new DateTime();
        }
        return $this->createQueryBuilder('c')
            ->andWhere('c.published_at > :first_datetime')
            ->andWhere('c.published_at < :last_datetime')
            ->orderBy('c.id','DESC')
            ->setParameter('first_datetime', $first_datetime)
            ->setParameter('last_datetime',$last_datetime)
            ->getQuery()
            ->getResult();
    }
    // /**
    //  * @return Comment[] Returns an array of Comment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Comment
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
