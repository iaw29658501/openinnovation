<?php

namespace App\Repository;

use App\Entity\Idea;
use DateTime;
use DateTimeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Idea|null find($id, $lockMode = null, $lockVersion = null)
 * @method Idea|null findOneBy(array $criteria, array $orderBy = null)
 * @method Idea[]    findAll()
 * @method Idea[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IdeaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Idea::class);
    }

    /**
     * @return Idea[] Returns an array of Idea objects
    */
    public function findAllByNewest(): array
    {
        return $this->createQueryBuilder('i')
            ->orderBy('i.published_at','DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param DateTimeInterface $first_datetime
     * @param DateTimeInterface|null $last_datetime
     * @return Idea[] Returns an array of Idea objects
     */
    public function publishedBetween(DateTimeInterface $first_datetime, DateTimeInterface $last_datetime = null): array
    {
        if ($last_datetime === null) {
            $last_datetime = new DateTime();
        }
        return $this->createQueryBuilder('i')
            ->andWhere('i.published_at > :first_datetime')
            ->andWhere('i.published_at < :last_datetime')
            ->orderBy('i.id','DESC')
            ->setParameter('first_datetime', $first_datetime)
            ->setParameter('last_datetime',$last_datetime)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param DateTimeInterface $first_datetime
     * @param DateTimeInterface|null $last_datetime
     * @return Idea[] Returns an array of Idea objects
     */
    public function publishedBetweenTop5MostLiked(DateTimeInterface $first_datetime, DateTimeInterface $last_datetime = null): array
    {
        if ($last_datetime === null) {
            $last_datetime = new DateTime();
        }
        return $this->createQueryBuilder('i')
            ->select('i')
            ->addSelect('COUNT( IDENTITY(il) ) AS likes_count')
            ->leftJoin('i.likes','il')
            ->where('i.published_at >= :date1')
            ->andWhere('i.published_at <= :date2')
            ->setParameter('date1',$first_datetime)
            ->setParameter('date2',$last_datetime)
            ->groupBy('i.id')
            ->orderBy('likes_count', 'DESC')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();
    }

    public function publishedBetweenOrderByRate(DateTime $first_datetime, DateTime $last_datetime, $analytic_note =null): array
    {
        if ($analytic_note === null) {
            $analytic_note = 'financial_note';
        }
        return $this->createQueryBuilder('i')
            ->leftJoin('i.notes','note')
            ->addSelect('avg(note.' . $analytic_note . ') AS avg_note')
            ->groupBy('i.id')
            ->orderBy('avg_note','DESC')
            ->where('i.published_at >= :date1')
            ->andWhere('i.published_at <= :date2')
            ->setParameter('date1',$first_datetime)
            ->setParameter('date2',$last_datetime)
            ->getQuery()
            ->getResult();
    }

    public function findAllOfThisUser(UserInterface $user)
    {
        return $this->createQueryBuilder('i')
            ->where('i.owner = :u')
            ->setParameter('u',$user)
            ->getQuery()
            ->getResult();
    }

    public function publishedBetweenOrderByLikes($first_datetime, $last_datetime)
    {
        return $this->createQueryBuilder('i')
            ->addSelect('COUNT(IDENTITY(il)) as likes_count')
            ->leftJoin('i.likes','il')
            ->where('i.published_at >= :date1')
            ->andWhere('i.published_at <= :date2')
            ->setParameter('date1',$first_datetime)
            ->setParameter('date2',$last_datetime)
            ->orderBy('likes_count','DESC')
            ->groupBy('i.id')
            ->getQuery()
            ->getResult();
    }

}
