<?php

namespace App\Repository;

use App\Entity\IdeaStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method IdeaStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method IdeaStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method IdeaStatus[]    findAll()
 * @method IdeaStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IdeaStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IdeaStatus::class);
    }

     /**
      * @return IdeaStatus[] Returns an array of IdeaStatus objects
      */

    public function findAllByStepNumber(): array
    {
        return $this->createQueryBuilder('i')
            ->orderBy('i.number', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findFirst(): ?IdeaStatus
    {
        return $this->createQueryBuilder('i')
            ->addOrderBy('i.number','ASC')
            ->addOrderBy('i.id','ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult()[0];
    }
}
