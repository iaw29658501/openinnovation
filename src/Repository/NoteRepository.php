<?php

namespace App\Repository;

use App\Entity\Idea;
use App\Entity\Note;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Note|null find($id, $lockMode = null, $lockVersion = null)
 * @method Note|null findOneBy(array $criteria, array $orderBy = null)
 * @method Note[]    findAll()
 * @method Note[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Note::class);
    }

    /**
     * @param UserInterface $user
     * @param Idea $idea
     * @return Note[] Returns an array of Note objects
     */

    public function findByUserAndIea( UserInterface $user, Idea $idea): array
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.idea = :idea')
            ->andWhere('n.user = :user')
            ->setParameter('idea', $idea)
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult()
        ;
    }

    public function userAlreadyVotedForThisIdea(UserInterface $user, \App\Entity\Idea $idea)
    {
        return $this->createQueryBuilder('n')
            ->where('n.idea = :idea_id')
            ->andWhere('n.user = :user_id')
            ->setParameter('idea_id',$idea->getId())
            ->setParameter('user_id', $user->getId())
            ->getQuery()
            ->getResult();
    }

    public function findByIdeaGroupBySector(Idea $idea)
    {
        return $this->createQueryBuilder('n')
            ->select('sectors.name')
            ->addSelect('count( n ) AS number_of_votes')
            ->addSelect('avg( n.financial_note ) AS avg_financial_note')
            ->addSelect('avg( n.urgency_note ) AS avg_urgency_note')
            ->leftJoin('n.user','user')
            ->leftJoin('user.sectors','sectors')
            ->andWhere('n.idea = :idea')
            ->setParameter('idea',$idea)
            ->groupBy('sectors')
            ->getQuery()
            ->getResult();
    }

    public function publishedBetween(\DateTime $date_search1, $date_search2)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.published_at > :first_datetime')
            ->andWhere('n.published_at < :last_datetime')
            ->setParameter('first_datetime', $date_search1)
            ->setParameter('last_datetime',$date_search2)
            ->getQuery()
            ->getResult();
    }
}
