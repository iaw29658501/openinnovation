<?php

namespace App\Repository;

use App\Entity\Sector;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Sector|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sector|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sector[]    findAll()
 * @method Sector[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SectorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sector::class);
    }

    /**
     * @param \DateTimeInterface $first_datetime
     * @param \DateTimeInterface $last_datetime
     * @return Sector[]
     */
    public function findWithCount(\DateTimeInterface $first_datetime, \DateTimeInterface $last_datetime): array
    {
        return $this->createQueryBuilder('s')
            ->select('s as sector')
            ->addSelect('count(si) AS count')
            ->leftJoin('s.ideas','si')
            ->andWhere('si.published_at >= :date1')
            ->andWhere('si.published_at <= :date2')
            ->setParameter('date1',$first_datetime)
            ->setParameter('date2',$last_datetime)
            ->groupBy('s')
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Sector[] Returns an array of Sector objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Sector
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

}
