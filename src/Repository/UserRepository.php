<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function lastRequestBetween(\DateTimeInterface $date_search1, \DateTimeInterface $date_search2)
    {
        return $this->createQueryBuilder('u')
            ->select('u')
            ->where('u.last_request >= :date1')
            ->andWhere('u.last_request <= :date2')
            ->setParameter('date1',$date_search1)
            ->setParameter('date2',$date_search2)
            ->getQuery()
            ->getResult();
    }

    //    public function count()
//    {
//        return $this->createQueryBuilder('u')
//            ->select('count(u.id)')
//            ->getQuery()
//            ->getSingleScalarResult();
//    }


    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function findAllOrderByIdeas()
    {
        return $this->createQueryBuilder('u')
            ->leftJoin('u.ideas','ui')
            ->groupBy('u.id')
            ->orderBy('count(ui)', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
