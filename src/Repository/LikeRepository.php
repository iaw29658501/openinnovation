<?php

namespace App\Repository;

use App\Entity\Idea;
use App\Entity\Like;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Like|null find($id, $lockMode = null, $lockVersion = null)
 * @method Like|null findOneBy(array $criteria, array $orderBy = null)
 * @method Like[]    findAll()
 * @method Like[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LikeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Like::class);
    }


    public function findOneByUserIdea(User $user, Idea $idea)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.idea = :idea')
            ->andWhere('l.user = :user')
            ->setParameter('idea', $idea->getId())
            ->setParameter('user', $user->getId())
            ->getQuery()
            ->getResult();
    }

    public function publishedBetween(\DateTimeInterface $date_search1, \DateTimeInterface $date_search2)
    {
        return $this->createQueryBuilder('l')
            ->select('l')
            ->where('l.datetime >= :date1')
            ->andWhere('l.datetime <= :date2')
            ->setParameter('date1',$date_search1)
            ->setParameter('date2',$date_search2)
            ->getQuery()
            ->getResult();
    }


    /*
    public function findOneBySomeField($value): ?Like
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
