<?php

namespace App\Form;

use App\Entity\Idea;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\File;

class IdeaType extends AbstractType
{
    /**
     * @var Security
     */
    private Security $security;

    /**
     * IdeaType constructor.
     * @param Security $security
     */
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',null,[
                'label' => false,
                'attr' => [
                    'placeholder'=> 'Nombre de la idea'
                ]
            ])
            ->add('title',null,[
                'label' => false,
                'attr' => [
                    'placeholder'=> 'Título'
                ]
            ])
            ->add('description',TextareaType::class,[
                'label' => false,
                'attr' => [
                    'placeholder'=> 'Descripción'
                ]
            ])
            ->add('tags',null,
            [
                'expanded'=>true,
                'attr'=>[
                    'class' => 'horizontal-boxes bg-white px-2 rounded',
                ]])
            ->add('sectors',null,
                [
                    'expanded'=>true,
                    'attr'=>[
                        'class' => 'horizontal-boxes bg-white px-2 rounded',
                    ]])
            ->add('anonymous',null,[
                'label'=>'Anonimizar mi nombre encuanto la idea no sea aprobada'
            ])
            ->add('files', FileType::class, [
                'label' => 'Archivos',
                'multiple'=> true,
                // unmapped means that this field is not associated to any entity property
                'mapped' => false,
                // make it optional so you don't have to re-upload the PDF file
                // every time you edit the Product details
                'required' => false,
                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    // files are multiple (array) so the constraint need to be go to All
                    new All([
                        'constraints'=> [
                            new File([
                                // TODO Check if your php.ini is well configured
                                'maxSize' => '25M',
                                'mimeTypes' => [
                                    'application/pdf',
                                    'application/x-pdf',
                                    'image/jpeg',
                                    'image/png',
                                    // TODO: test against MACROS phishing
                                    'application/msword',
                                    'application/vnd.ms-excel',
                                    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                    'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                                    'text/plain',
                                    'application/x-rar-compressed',
                                    'application/zip'
                                ],
                                'mimeTypesMessage' => 'Formato de archivo inválido',
                            ])
                        ]
                    ])

                ],
            ]);
            if ($this->security->isGranted('ROLE_IDEA_STATUS')) {
                $builder->add('status',null);
            }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Idea::class
        ]);
    }
}
