<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('roles',ChoiceType::class,[
                'expanded'=>true,
                'choices' => [
                    'Ver todas las ideas' => 'ROLE_VIEWER',
                    'Crear cualquier idea' => 'ROLE_IDEA_NEW',
                    'Evaluar cualquier idea' => 'ROLE_NOTE_NEW',
                    'Comentar ideas' => 'ROLE_COMMENT_NEW',
                    // TODO: cambiar estado de idea sin poder editarla toda
                    'Editar todos usuarios'=>'ROLE_USER_FULL',
                    'Editar todas tags'=>'ROLE_TAG_FULL',
                    'Editar todos estados de ideas'=>'ROLE_IDEA_EDIT',
                    'Editar todos comentarios'=>'ROLE_COMMENT_FULL',
                    'Editar todas evaluaciones'=>'ROLE_NOTE_FULL',
                    'Administrar todo'=>'ROLE_ADMIN'
                ],
                'required'=>true,
                'multiple'=>true
            ])
            ->add('plainPassword',PasswordType::class, [
                'mapped'=>false,
                'label'=>'contraseña'
            ])
           // ->add('last_request')
            ->add('name',null,[
                'label'=>'Nombre'
           ])
            ->add('sectors',null,[
                'expanded'=>true,
                'label'=>'Sectores'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
