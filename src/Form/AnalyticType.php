<?php

namespace App\Form;

use App\Entity\Analytic;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnalyticType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('GET')
            ->add('date_1',null,
            [
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr'=>[
                    'class'=>'js-datepicker'
                ],
                'html5'=>false
            ]
            )
            ->add('date_2',null,
                [
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr'=>[
                        'class'=>'js-datepicker'
                    ],
                    'html5'=>false
                ]
            )

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Analytic::class,
            'csrf_protection' => false,
        ]);
    }
}
