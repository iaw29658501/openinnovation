<?php

namespace App\Entity;

use App\Repository\TagRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TagRepository::class)
 */
class Tag
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=18)
     */
    private ?string $name;

    /**
     * @ORM\ManyToMany(targetEntity=Idea::class, mappedBy="tags", cascade={"persist"})
     */
    private Collection $ideas;

    public function __construct()
    {
        $this->ideas = new ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->getName();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

//    /**
//     * @return Collection|Idea[]
//     */
//    public function getIdeas(): Collection
//    {
//        return $this->ideas;
//    }
//
//    public function addIdea(Idea $idea): self
//    {
//        if (!$this->ideas->contains($idea)) {
//            $this->ideas[] = $idea;
//        }
//
//        return $this;
//    }
//
//    public function removeIdea(Idea $idea): self
//    {
//        $this->ideas->removeElement($idea);
//
//        return $this;
//    }

/**
 * @return Collection|Idea[]
 */
public function getIdeas(): Collection
{
    return $this->ideas;
}

public function addIdea(Idea $idea): self
{
    if (!$this->ideas->contains($idea)) {
        $this->ideas[] = $idea;
        $idea->addTag($this);
    }

    return $this;
}

public function removeIdea(Idea $idea): self
{
    if ($this->ideas->removeElement($idea)) {
        $idea->removeTag($this);
    }

    return $this;
}
}
