<?php

namespace App\Entity;

use App\Repository\IdeaRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=IdeaRepository::class)
 */
class Idea
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=63)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=127)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=8191 , nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="ideas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @ORM\Column(type="datetime")
     */
    private $published_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $last_update_at;

    /**
     * @ORM\ManyToMany(targetEntity=Tag::class, inversedBy="ideas", cascade={"persist"})
     * @Assert\Count(
     *     max = "4",
     *     maxMessage="Máximo de 4 tags"
     * )
     */
    private $tags;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="idea", orphanRemoval=true)
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity=Like::class, mappedBy="idea", orphanRemoval=true)
     */
    private $likes;

    /**
     * @ORM\ManyToMany(targetEntity=Sector::class, mappedBy="ideas")
     * @Assert\Count(
     *     max = "3",
     *     maxMessage="Máximo de 3 sectores"
     * )
     */
    private $sectors;

    /**
     * @ORM\Column(type="boolean")
     */
    private $anonymous;

    /**
     * @ORM\ManyToOne(targetEntity=IdeaStatus::class, inversedBy="ideas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity=File::class, mappedBy="idea", orphanRemoval=true)
     */
    private $files;

    /**
     * @ORM\OneToMany(targetEntity=Note::class, mappedBy="idea", orphanRemoval=true)
     */
    private $notes;

    /**
     * @ORM\Column(type="datetime")
     */
    private $last_status_change;


    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->likes = new ArrayCollection();
        $this->sectors = new ArrayCollection();
        $this->published_at = new DateTime();
        $this->last_update_at = new DateTime();
        $this->anonymous=false;
        $this->files = new ArrayCollection();
        $this->notes = new ArrayCollection();
        $this->last_status_change = new DateTime();
        $this->status = null;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;
        return $this;
    }

    public function getPublishedAt(): ?DateTimeInterface
    {
        return $this->published_at;
    }

    public function setPublishedAt($published_at): self
    {
        $this->published_at = $published_at;
        return $this;
    }

    public function getLastUpdateAt(): ?DateTimeInterface
    {
        return $this->last_update_at;
    }

    public function setLastUpdateAt($last_update_at): self
    {
        $this->last_update_at = $last_update_at;
        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setIdea($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getIdea() === $this) {
                $comment->setIdea(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return (string) $this->getName();
    }

    /**
     * @return Collection|Like[]
     */
    public function getLikes(): Collection
    {
        return $this->likes;
    }

    public function addLike(Like $like): self
    {
        if (!$this->likes->contains($like)) {
            $this->likes[] = $like;
            $like->setIdea($this);
        }

        return $this;
    }

    public function removeLike(Like $like): self
    {
        if ($this->likes->removeElement($like)) {
            // set the owning side to null (unless already changed)
            if ($like->getIdea() === $this) {
                $like->setIdea(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Sector[]
     */
    public function getSectors(): Collection
    {
        return $this->sectors;
    }

    public function addSector(Sector $sector): self
    {
        if (!$this->sectors->contains($sector)) {
            $this->sectors[] = $sector;
            $sector->addIdea($this);
        }

        return $this;
    }

    public function removeSector(Sector $sector): self
    {
        if ($this->sectors->removeElement($sector)) {
            $sector->removeIdea($this);
        }

        return $this;
    }


    public function getAnonymous(): ?bool
    {
        return $this->anonymous;
    }

    public function setAnonymous(bool $anonymous): self
    {
        $this->anonymous = $anonymous;

        return $this;
    }

    public function getStatus(): ?IdeaStatus
    {
        return $this->status;
    }

    public function setStatus(?IdeaStatus $status): self
    {
        if ($this->getStatus() !== $status) {
            $this->setLastStatusChange(new DateTime());
        }
        $this->status = $status;
        return $this;
    }

    /**
     * @return Collection|File[]
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function addFile(File $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
            $file->setIdea($this);
        }

        return $this;
    }

    public function removeFile(File $file): self
    {
        if ($this->files->removeElement($file)) {
            // set the owning side to null (unless already changed)
            if ($file->getIdea() === $this) {
                $file->setIdea(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Note[]
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    public function addNote(Note $note): self
    {
        if (!$this->notes->contains($note)) {
            $this->notes[] = $note;
            $note->setIdea($this);
        }

        return $this;
    }

    public function removeNote(Note $note): self
    {
        if ($this->notes->removeElement($note)) {
            // set the owning side to null (unless already changed)
            if ($note->getIdea() === $this) {
                $note->setIdea(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        $this->tags->removeElement($tag);

        return $this;
    }

    public function getLastStatusChange(): ?\DateTimeInterface
    {
        return $this->last_status_change;
    }

    public function setLastStatusChange(\DateTimeInterface $last_status_change): self
    {
        $this->last_status_change = $last_status_change;

        return $this;
    }

}
