<?php

namespace App\Entity;

use App\Repository\NoteRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=NoteRepository::class)
 */
class Note
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="notes")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?User $user;

    /**
     * @ORM\ManyToOne(targetEntity=Idea::class, inversedBy="notes")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\Range(
     *      min = 1,
     *      max = 10
     * )
     *
     */
    private ?Idea $idea;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(
     *      min = 1,
     *      max = 10
     * )
     */
    private ?int $financial_note;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $urgency_note;

    /**
     * @ORM\Column(type="string", length=511, nullable=true)
     */
    private ?string $comment;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $published_at;

    public function __construct()
    {
        $this->published_at = new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getIdea(): ?Idea
    {
        return $this->idea;
    }

    public function setIdea(?Idea $idea): self
    {
        $this->idea = $idea;

        return $this;
    }

    public function getFinancialNote(): ?int
    {
        return $this->financial_note;
    }

    public function setFinancialNote(int $financial_note): self
    {
        $this->financial_note = $financial_note;

        return $this;
    }

    public function getUrgencyNote(): ?int
    {
        return $this->urgency_note;
    }

    public function setUrgencyNote(int $urgency_note): self
    {
        $this->urgency_note = $urgency_note;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getPublishedAt(): ?DateTimeInterface
    {
        return $this->published_at;
    }

    public function setPublishedAt(DateTimeInterface $published_at): self
    {
        $this->published_at = $published_at;
        return $this;
    }

    public function __toString(): string
    {
        return $this->getUser()->getName() . ' ' . $this->getFinancialNote() . ' ';
    }

}
