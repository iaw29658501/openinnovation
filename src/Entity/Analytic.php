<?php

namespace App\Entity;

use App\Repository\AnalyticRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=AnalyticRepository::class)
 */
class Analytic
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\Range( min="-5 years", max="now" )
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_1;

    /**
     * @Assert\Range( min="-5 years", max="now" )
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_2;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate1(): ?\DateTimeInterface
    {
        return $this->date_1;
    }

    public function setDate1(?\DateTimeInterface $date_1): self
    {
        $this->date_1 = $date_1;

        return $this;
    }

    public function getDate2(): ?\DateTimeInterface
    {
        return $this->date_2;
    }

    public function setDate2(?\DateTimeInterface $date_2): self
    {
        $this->date_2 = $date_2;

        return $this;
    }
}
