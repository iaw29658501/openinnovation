<?php

namespace App\Controller;

use App\Entity\Idea;
use App\Entity\Note;
use App\Form\NoteType;
use App\Repository\NoteRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/note")
 * @Security("is_granted('ROLE_USER')")
 */
class NoteController extends AbstractController
{
    /**
     * @Route("/", name="note_index", methods={"GET"})
     * @Security("is_granted('ROLE_NOTE_SHOW')")
     * @param NoteRepository $noteRepository
     * @return Response
     */
    public function index(NoteRepository $noteRepository): Response
    {
        return $this->render('note/index.html.twig', [
            'notes' => $noteRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new/{idea_id}", name="note_new", methods={"GET","POST"})
     * @ParamConverter("idea",options={"id" = "idea_id" })
     * @Security("is_granted('IDEA_NOTE', idea)")
     * @param Request $request
     * @param Idea $idea
     * @return Response
     */
    public function new(Request $request, Idea $idea, NoteRepository $noteRepository): Response
    {
        $note = new Note();
        $form = $this->createForm(NoteType::class, $note);
        $form->handleRequest($request);

        $hasVoted = $noteRepository->findByUserAndIea($this->getUser(),$idea);
        if ($hasVoted) {
            return $this->redirectToRoute('note_edit', [ 'id'=>$hasVoted[0]->getId() ]);
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $note->setUser($this->getUser());
            $note->setIdea($idea);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($note);
            $entityManager->flush();

            return $this->redirectToRoute('idea_show',['id'=>$idea->getId()]);
        }

        return $this->render('note/new.html.twig', [
            'note' => $note,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}", name="note_show", methods={"GET"})
     * @Security("is_granted('ROLE_NOTE_SHOW')")
     * @param Note $note
     * @return Response
     */
    public function show(Note $note): Response
    {
        return $this->render('note/show.html.twig', [
            'note' => $note,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="note_edit", methods={"GET","POST"})
     * @Security("is_granted('ROLE_NOTE_EDIT')")
     * @param Request $request
     * @param Note $note
     * @return Response
     */
    public function edit(Request $request, Note $note): Response
    {
        $form = $this->createForm(NoteType::class, $note);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('note_index');
        }

        return $this->render('note/edit.html.twig', [
            'note' => $note,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="note_delete", methods={"DELETE"})
     * @Security("is_granted('ROLE_NOTE_DELETE')")
     * @param Request $request
     * @param Note $note
     * @return Response
     */
    public function delete(Request $request, Note $note): Response
    {
        if ($this->isCsrfTokenValid('delete'.$note->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($note);
            $entityManager->flush();
        }

        return $this->redirectToRoute('note_index');
    }
}
