<?php

namespace App\Controller;

use App\Entity\File;
use App\Entity\Idea;
use App\Service\UploadHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/file")
 */
class FileController extends AbstractController
{

    /**
     * @Route("/add-to-idea/{id}", name="idea_add_file", methods={"GET","POST"})
     * @Security("is_granted('IDEA_EDIT', idea )")
     * @param Request $request
     * @param Idea $idea
     * @param UploadHelper $uploadHelper
     * @return Response
     * @throws \Exception
     */
    public function new(Request $request, Idea $idea, UploadHelper $uploadHelper): Response
    {
        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $request->files->get('file');
        $file = $uploadHelper->uploadIdeaFile($uploadedFile, $idea);
        // TODO: return file upload violation to dropzone
        //        if ($violations->count() > 0) {
        //            return $this->json($violations,400);
        //        }
        // Make $file serializable to return it
        // return $this->json($file,200);
        return $this->json($file,200);
    }

    // TODO: only people that can go to edit can get the CSRF token for this, is this enough ?
    /**
     * @Route("/delete/{id}", name="file_delete", methods={"DELETE"})
     * @param Request $request
     * @param File $file
     * @return Response
     */
    public function delete(Request $request, File $file): Response
    {
        if ($this->isCsrfTokenValid('delete'.$file->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($file);
            $entityManager->flush();
        }

        return $this->json([
            'success' => 'true'
        ]);
    }

//    /**
//     * @Route("/{id}", name="file_show", methods={"GET"})
//     */
//    public function show(File $file): Response
//    {
//        return $this->render('file/show.html.twig', [
//            'file' => $file,
//        ]);
//    }

}
