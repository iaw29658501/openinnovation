<?php

namespace App\Controller;

use App\Entity\Analytic;
use App\Entity\Idea;
use App\Form\AnalyticType;
use App\Repository\CommentRepository;
use App\Repository\IdeaRepository;
use App\Repository\LikeRepository;
use App\Repository\NoteRepository;
use App\Repository\SectorRepository;
use App\Repository\TagRepository;
use App\Repository\UserRepository;
use DateInterval;
use DatePeriod;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/analytics")
 * @Security("is_granted('ROLE_ANALYTIC')")
 */
class AnalyticController extends AbstractController
{
    public array $colors = ["#5390d9","#7400b8","#56cfe1","#0cd157","#c45850","#3cba9f","#eeff00","#ff7b00", "#0d8941","#845EC2","#D65DB1","#C4FCEF","#B0A8B9","#5390d9","#7400b8","#56cfe1","#0cd157","#c45850","#3cba9f","#eeff00","#ff7b00", "#0d8941","#845EC2","#D65DB1","#C4FCEF","#B0A8B9"];

    /**
     * @Route("/", name="analytic_index")
     * @param Request $request
     * @param UserRepository $userRepository
     * @param IdeaRepository $ideaRepository
     * @param CommentRepository $commentRepository
     * @param LikeRepository $likeRepository
     * @return Response
     */
    public function index(Request $request, UserRepository $userRepository, IdeaRepository $ideaRepository, CommentRepository $commentRepository, LikeRepository $likeRepository, NoteRepository $noteRepository): Response
    {
        $dates = $this->datesFromRequest($request);
        $date_search1 = $dates['begin'];
        $date_search2 = $dates['end'];
        $all_users = $userRepository->findAll();
        // TODO high: last request maybe today but the user may logged yesterday too
        $users_that_interacted_between_dates = $userRepository->lastRequestBetween($date_search1, $date_search2);
        $ideas_between_dates = $ideaRepository->publishedBetween($date_search1,$date_search2);
        $comments_between_dates = $commentRepository->publishedBetween($date_search1, $date_search2);
        $likes_between_dates = $likeRepository->publishedBetween($date_search1, $date_search2);
        $ideas_between_dates_top5_most_liked = $ideaRepository->publishedBetweenTop5MostLiked($date_search1,$date_search2);
        $ideas_most_economy_rate = $ideaRepository->publishedBetweenOrderByRate($date_search1, $date_search2,'financial_note');
        $ideas_most_urgency_rate = $ideaRepository->publishedBetweenOrderByRate($date_search1, $date_search2,'urgency_note');
        $new_reviews = $noteRepository->publishedBetween($date_search1,$date_search2);
        return $this->render('analytic/index.html.twig', [
            'controller_name' => 'AnalyticController',
            'form'=>$dates['form']->createView(),
            'date_search'=>[$date_search1,$date_search2],
            'users' => $all_users,
            'users_using_app_since_x' => $users_that_interacted_between_dates,
            'ideas_since_x' => $ideas_between_dates,
            'comments_since_x' => $comments_between_dates,
            'likes_since_x' => $likes_between_dates,
            'ideas_most_liked'=>$ideas_between_dates_top5_most_liked,
            'ideas_most_economy_rate'=>$ideas_most_economy_rate,
            'ideas_most_urgency_rate'=>$ideas_most_urgency_rate,
            'new_reviews'=>$new_reviews
        ]);
    }

    /**
     * @Route("/idea/{id}", name="analytic_idea")
     * @param Idea $idea
     * @return Response
     */
    public function forIdea(Idea $idea): Response
    {
        return $this->render('analytic/idea.html.twig',[
            'idea'=>$idea
        ]);
    }

    /**
     * @Route("/data/idea/{id}/notes_by_sector", name="notes_by_sector")
     * @param Idea $idea
     * @param NoteRepository $noteRepository
     * @return JsonResponse
     */
    public function dataIdeaNotesBySector(Idea $idea, NoteRepository $noteRepository): JsonResponse
    {
        $notes = $noteRepository->findByIdeaGroupBySector($idea);
        return $this->json([
            // usuarios sin un sector se mostraran como sector 'ningun' en vez de null
            'labels' =>  array_map(function($v) { return (is_null($v)) ? 'ningún':$v; },array_column($notes,'name')),
            'datasets' => [
                [
                    'label'=>'Evaluación Financiera',
                    'backgroundColor'=> "#3e95cd",
                    'data'=>array_column($notes,'avg_financial_note'),
                ],
                [
                    'label'=>'Evaluación Estrella',
                    'backgroundColor'=> "#8e5ea2",
                    'data'=>array_column($notes,'avg_urgency+_note'),
                ]
            ]
        ]);
    }

    // Gives the number of new ideas and comments between two dats,
    // return json to Charts.js draw the data
    /**
     * @Route("/data/chart", name="analytic_general")
     * @param IdeaRepository $ideaRepository
     * @param CommentRepository $commentRepository
     * @param Request $request
     * @return JsonResponse
     */
    public function generalAnalytics(IdeaRepository $ideaRepository, CommentRepository $commentRepository, Request $request): JsonResponse
    {
        $datesInput=$this->getDates($request);
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($datesInput['begin'], $interval, $datesInput['end']);
        $data=[];
        foreach ($period as $key => $dt) {

            $dt_end = (clone $dt)->add($interval)->modify('-1 second');
            $data['ideas'][] = [
                'date'=>$dt->format('d/m'),
                'count'=>count($ideaRepository->publishedBetween($dt,$dt_end))
            ];
            $data['comments'][]=[
                'date'=>$dt->format('d/m'),
                'count'=>count($commentRepository->publishedBetween($dt,$dt_end))
            ];
        }
        return $this->json([
            'labels' =>  array_column($data['ideas'],'date'),
            'datasets' => [
                [
                    'label'=>'Ideas Nuevas',
                    'data'=>array_column($data['ideas'],'count'),
                    'fill'=>false,
                    'borderColor'=>'#00ff00'
                ],
                [
                    'label'=>'Comentarios',
                    'data'=>array_column($data['comments'],'count'),
                    'fill'=>false,
                    'borderColor'=>'#0000ff'
                ]
            ]
        ]);
    }

    /**
     * @Route("/data/sector", name="analytic_sector_pie")
     * @param Request $request
     * @param SectorRepository $sectorRepository
     * @return JsonResponse
     */
    public function sector(Request $request, SectorRepository $sectorRepository): JsonResponse
    {
        $dates = $this->getDates($request);
        $sectors_with_count = $sectorRepository->findWithCount($dates['begin'],$dates['end']);
        // TODO low: refactor this please
        $sectors_name=[];
        foreach ($sectors_with_count as $sector) {
            $sectors_name[] = $sector['sector']->getName();
        }
        return $this->json([
            'labels' => $sectors_name,
            'datasets'=> [
                [
                    'label'=> "Ideas a cada sector",
                    'backgroundColor'=> $this->colors,
                    'data'=> array_column($sectors_with_count,'count')
                ]
            ]
        ]);
    }

    /**
     * @Route("/data/tags", name="analytic_tags_pie")
     * @param Request $request
     * @param TagRepository $tagRepository
     * @return JsonResponse
     */
    public function tags(Request $request, TagRepository $tagRepository): JsonResponse
    {
        $dates = $this->getDates($request);
        $tags_with_count = $tagRepository->findWithCount($dates["begin"],$dates["end"] );
        // TODO low: refactor this please
        foreach ($tags_with_count as $tag_with_count) {
            $tag_name[] = $tag_with_count['tag']->getName();
        }
        return $this->json([
            'labels' => $tag_name,
            'datasets'=> [
                [
                    'label'=> "Ideas por tags",
                    'backgroundColor'=> $this->colors,
                    'data'=> array_column($tags_with_count,'counter')
                ]
            ]
        ]);
    }

    private function getDates(Request $request, $day=null,$month=null,$year=null,$day2=null,$month2=null,$year2=null): array
    {
        // TODO low: refactor with form builder?
        $dates=$request->get('dates');
        if (!$dates) {
            $begin = DateTime::createFromFormat('d/m/y',$day.'/'.$month.'/'.$year);
            $end = DateTime::createFromFormat('d/m/y',$day2.'/'.$month2.'/'.$year2);
        } else {
            $begin    =   DateTime::createFromFormat("d/m/y",$dates[0])->setTime(0,0,0);
            $end    =   DateTime::createFromFormat("d/m/y",$dates[1])->setTime(23,59);
        }
        return ['begin'=>$begin,'end'=>$end];
    }

    /**
     * @Route ("/ideas_between_dates/",name="analytic_new_ideas")
     * @param Request $request
     * @param IdeaRepository $ideaRepository
     * @return Response
     */
    public function newIdeas(Request $request, IdeaRepository $ideaRepository): Response
    {
        $dates=$this->datesFromRequest($request);
        if ($request->get('orderBy') === 'economy') {
            $ideas = $ideaRepository->publishedBetweenOrderByRate($dates['begin'],$dates['end'],'financial_note');
            return $this->render('idea/index_with_note.html.twig',[
                'ideas'=>$ideas
            ]);
        }

        if ($request->get('orderBy') === 'urgency') {
            $ideas = $ideaRepository->publishedBetweenOrderByRate($dates['begin'],$dates['end'],'urgency_note');
            return $this->render('idea/index_with_note.html.twig',[
                'ideas'=>$ideas
            ]);
        }

        if ($request->get('orderBy') === 'likes') {
            $ideas = $ideaRepository->publishedBetweenOrderByLikes($dates['begin'],$dates['end'],'urgency_note');
            return $this->render('idea/index_with_note.html.twig',[
                'ideas'=>$ideas
            ]);
        }

        $ideas = $ideaRepository->publishedBetween($dates['begin'],$dates['end']);
        return $this->render('idea/index.html.twig',[
            'ideas'=>$ideas
        ]);
    }

    /**
     * @Route ("/comments_between_dates/",name="analytic_comments_between_dates")
     * @param Request $request
     * @param CommentRepository $commentRepository
     * @return Response
     */
    public function commentsBetweenDates(Request $request, CommentRepository $commentRepository): Response
    {
        $dates=$this->datesFromRequest($request);
        $comments = $commentRepository->publishedBetween($dates['begin'],$dates['end']);
        return $this->render('comment/index.html.twig',[
            'comments'=>$comments
        ]);
    }

    /**
     * @Route ("/users_between_dates/",name="analytic_users_between_dates")
     * @param Request $request
     * @param CommentRepository $commentRepository
     * @return Response
     */
    public function usersBetweenDates(Request $request, UserRepository $userRepository): Response
    {
        $dates=$this->datesFromRequest($request);
        $users = $userRepository->lastRequestBetween($dates['begin'],$dates['end']);
        return $this->render('user/index.html.twig',[
            'users'=>$users
        ]);
    }


    private function datesFromRequest(Request $request): array
    {
        $date_search1 = $default_date1 = (new DateTime());
        $date_search2 = $default_date2 = (new DateTime())->modify('-7 day');
        $analytic = new Analytic();
        $analytic->setDate1($date_search1);
        $analytic->setDate2($date_search2);
        $form = $this->createForm(AnalyticType::class,$analytic);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if($form->isValid()) {
                $date_search1 = $form->get('date_1')->getData();
                $date_search2 = $form->get('date_2')->getData();
            } else {
                $this->addFlash('error','Fechas inválidas, volviendo a los padrones por defecto');
            }
        }
        if($date_search1 > $date_search2) {
            $temp = $date_search1;
            $date_search1 = $date_search2;
            $date_search2 = $temp;
        }
        try {
            $date_search1 = new DateTime($date_search1->format("Y-m-d") . " 00:00:00");
            $date_search2 = new DateTime($date_search2->format("Y-m-d")." 23:59:59");
        } catch (\Exception $e) {
            $this->addFlash('error','Fechas inválidas, volviendo a los padrones por defecto');
            $this->redirectToRoute('analytic_index');
        }
        return ['begin'=>$date_search1,'end'=>$date_search2,'form'=>$form];
    }
}
