<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Idea;
use App\Entity\IdeaStatus;
use App\Entity\Like;
use App\Entity\Sector;
use App\Entity\Tag;
use App\Form\CommentType;
use App\Form\IdeaType;
use App\Repository\IdeaRepository;
use App\Service\UploadHelper;
use DateTime;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/ideas")
 */
class IdeaController extends AbstractController
{
    /**
     * @Route("/", name="idea_index", methods={"GET"})
     * @Security("is_granted('ROLE_IDEA_SHOW')")
     * @param IdeaRepository $ideaRepository
     * @param Request $request
     * @return Response
     */
    public function index(IdeaRepository $ideaRepository, Request $request): Response
    {
        $ideas = $ideaRepository->findAllByNewest();
        $ideas = $this->checkAnonFilter($ideas, $request);
        return $this->render('idea/index.html.twig', [
            'ideas' => $ideas,
            'sideBar'=> $this->sideBar()
        ]);
    }

    /**
     * @Route("/nueva", name="idea_new", methods={"GET","POST"})
     * @Security("is_granted('ROLE_IDEA_NEW')")
     * @param Request $request
     * @param UploadHelper $uploadHelper
     * @return Response
     * @throws Exception
     */
    public function new(Request $request, UploadHelper $uploadHelper): Response
    {
        $idea = new Idea();
        $idea->setStatus($this->getDoctrine()->getRepository(IdeaStatus::class)->findFirst());
        $form = $this->createForm(IdeaType::class, $idea);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $idea->setOwner($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            /** @var UploadedFile[] $uploadedFiles */
            $uploadedFiles = $form->get('files')->getData();
            // file are not required
            if ($uploadedFiles) {
                foreach ($uploadedFiles as $file) {
                    $uploadHelper->uploadIdeaFile($file, $idea);
                }
                // updates the file property to store the file entity ( with file name and idea that it belongs)
                // instead of the file contents
            }
            $idea->setStatus($entityManager->getRepository(IdeaStatus::class)->findBy(array(),array('id'=>'ASC'),1,0)[0]);
            foreach($idea->getTags() as $tag) {
                $tag->addIdea($idea);
                $entityManager->persist($tag);
            }
            foreach($idea->getSectors() as $sector) {
                $sector->addIdea($idea);
                $entityManager->persist($sector);
            }

            $entityManager->persist($idea);
            $entityManager->flush();
            $this->addFlash('success','Idea creada ¡Eres increíble! Gracias');
            $party=true;
            return $this->redirectToRoute('idea_index',[
                'party'=>$party
            ]);
        }

        return $this->render('idea/new.html.twig', [
            'idea' => $idea,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="idea_show", methods={"GET"})
     * @Security("is_granted('IDEA_VIEW', idea)")
     * @param Idea $idea
     * @return Response
     */
    public function show(Idea $idea): Response
    {
        $comment = new Comment();
        $form = $this
            ->createForm(CommentType::class, $comment)
            ->createView();
        return $this->render('idea/show.html.twig', [
            'idea' => $idea,
            'form'=>$form
        ]);
    }

    /**
     * @Route("/{id}/editar", name="idea_edit", methods={"GET","POST"})
     * @Security("is_granted('IDEA_EDIT',idea)")
     * @param Request $request
     * @param Idea $idea
     * @return Response
     */
    public function edit(Request $request, Idea $idea): Response
    {
        $form = $this->createForm(IdeaType::class, $idea);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $idea->setLastUpdateAt(new DateTime());
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('idea_index');
        }

        return $this->render('idea/edit.html.twig', [
            'idea' => $idea,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="idea_delete", methods={"DELETE"})
     * @Security("is_granted('IDEA_EDIT',idea)")
     * @param Request $request
     * @param Idea $idea
     * @return Response
     */
    public function delete(Request $request, Idea $idea): Response
    {
        if ($this->isCsrfTokenValid('delete'.$idea->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($idea);
            $entityManager->flush();
        }

        return $this->redirectToRoute('idea_index');
    }

    /**
     * @Route("/tag/{tag}", name="idea_by_tag", methods={"GET"})
     * @param Tag $tag
     * @param Request $request
     * @return Response
     */
    public function tag(Tag $tag, Request $request): Response
    {
        $ideas = $tag->getIdeas();
        $ideas = $this->checkAnonFilter($ideas, $request);
        return $this->render('idea/index.html.twig', [
            'ideas' => $ideas,
            'sideBar'=> $this->sideBar(),
        ]);
    }

    /**
     * @Route("/sector/{sector}", name="idea_by_sector", methods={"GET"})
     * @param Sector $sector
     * @param Request $request
     * @return Response
     */
    public function sector(Sector $sector, Request $request): Response
    {
        $ideas = $sector->getIdeas();
        $ideas = $this->checkAnonFilter($ideas, $request);
        return $this->render('idea/index.html.twig', [
            'ideas' => $ideas,
            'sideBar'=> $this->sideBar(),
        ]);
    }

    /**
     * @Route("/status/{status}", name="idea_by_status", methods={"GET"})
     * @param IdeaStatus $status
     * @param Request $request
     * @return Response
     */
    public function status(IdeaStatus $status, Request $request): Response
    {
        $ideas = $status->getIdeas();
        $ideas = $this->checkAnonFilter($ideas, $request);
        return $this->render('idea/index.html.twig', [
            'ideas' => $ideas,
            'sideBar'=> $this->sideBar(),
        ]);
    }

    /**
     * @Route("/{id}/change_like", name="idea_change_like", methods={"GET"})
     * @Security("is_granted('IDEA_VIEW',idea) and is_granted('ROLE_LIKE_NEW')")
     * @param Request $request
     * @param Idea $idea
     * @param UserInterface $user
     * @return Response
     */
    public function change_like(Request $request, Idea $idea, UserInterface $user): Response
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder()
            ->select('l')
            ->from(Like::class,'l')
            ->where('l.idea = :idea')
            ->andWhere('l.user = :user')
            ->setParameter('idea', $idea->getId())
            ->setParameter('user', $user->getId());
        if ($this->getUser() && $this->isCsrfTokenValid($this->getUser()->getId(), $request->get('_token'))) {
            // if already liked dislike it
            $likes = $qb->getQuery()->getResult();
            $count = count($likes);
            if ($count) {
                //$entityManager = $this->getDoctrine()->getManager();
                $em->remove($likes[0]);
                $em->flush();
            } else {
                //if not liked like it
                $em = $this->getDoctrine()->getManager();
                $like = (new Like())
                    ->setUser($user)
                    ->setIdea($idea)
                    ->setDatetime(new DateTime());
                $em->persist($like);
                $em->flush();
            }
            return $this->json([
                'count' => $idea->getLikes()->count()
            ]);
        }

        $this->addFlash("error","Invalid request");
        return $this->redirectToRoute('idea_index');
    }

    private function sideBar(): array
    {
        $doc = $this->getDoctrine();
        $tags = $doc->getRepository(Tag::class)->findAll();
        $sectors = $doc->getRepository(Sector::class)->findAll();
        $states = $doc->getRepository(IdeaStatus::class)->findAllByStepNumber();
        return [
            'tags'=>$tags,
            'sectors'=>$sectors,
            'states'=>$states
        ];
    }

    /**
     * @param $ideas
     * @param Request $request
     * @return mixed
     */
    private function checkAnonFilter($ideas, Request $request)
    {
        if ($this->container->get('security.authorization_checker')->isGranted('ROLE_ANONYMOUS_BYPASS')) {
            $anon = $request->get('anon');
            if (!$anon || $anon === 'default') {
                return $ideas;
            }

            if ($anon === 'on' || $anon === 'off') {
                $is_anon = $anon === 'on';
                foreach ($ideas as $idea) {
                    $idea->setAnonymous($is_anon);
                }
                return $ideas;
            }
        }
        return $ideas;
    }

}
