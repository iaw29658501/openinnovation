<?php

namespace App\Controller;

use App\Entity\IdeaStatus;
use App\Form\IdeaStatusType;
use App\Repository\IdeaStatusRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/idea-status")
 */
class IdeaStatusController extends AbstractController
{
    /**
     * @Route("/", name="idea_status_index", methods={"GET"})
     * @Security("is_granted('ROLE_STATUS_SHOW')")
     * @param IdeaStatusRepository $ideaStatusRepository
     * @return Response
     */
    public function index(IdeaStatusRepository $ideaStatusRepository): Response
    {
        return $this->render('idea_status/index.html.twig', [
            'idea_statuses' => $ideaStatusRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="idea_status_new", methods={"GET","POST"})
     * @Security("is_granted('ROLE_STATUS_NEW')")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $ideaStatus = new IdeaStatus();
        $form = $this->createForm(IdeaStatusType::class, $ideaStatus);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ideaStatus);
            $entityManager->flush();

            return $this->redirectToRoute('idea_status_index');
        }

        return $this->render('idea_status/new.html.twig', [
            'idea_status' => $ideaStatus,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="idea_status_show", methods={"GET"})
     * @Security("is_granted('ROLE_STATUS_SHOW')")
     * @param IdeaStatus $ideaStatus
     * @return Response
     */
    public function show(IdeaStatus $ideaStatus): Response
    {
        return $this->render('idea_status/show.html.twig', [
            'idea_status' => $ideaStatus,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="idea_status_edit", methods={"GET","POST"})
     * @Security("is_granted('ROLE_STATUS_EDIT')")
     * @param Request $request
     * @param IdeaStatus $ideaStatus
     * @return Response
     */
    public function edit(Request $request, IdeaStatus $ideaStatus): Response
    {
        $form = $this->createForm(IdeaStatusType::class, $ideaStatus);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('idea_status_index');
        }

        return $this->render('idea_status/edit.html.twig', [
            'idea_status' => $ideaStatus,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="idea_status_delete", methods={"DELETE"})
     * @Security("is_granted('ROLE_STATUS_DELETE')")
     * @param Request $request
     * @param IdeaStatus $ideaStatus
     * @return Response
     */
    public function delete(Request $request, IdeaStatus $ideaStatus): Response
    {
        if ($this->isCsrfTokenValid('delete'.$ideaStatus->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ideaStatus);
            $entityManager->flush();
        }

        return $this->redirectToRoute('idea_status_index');
    }
}
