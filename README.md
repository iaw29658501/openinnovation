# OpenInnovation
## Why and what is this ?
This web app is designed to be a central place to manage new ideas for your company, most apps that do this are not opensource so you cannot own your own data and they are very expensive, with this you can manage your employers, sectors and roles. You can share your ideas, comment them, like, organize ideas by sector or general tags, give them evaluations, change their status (and also create new ones) and you can attach files to them
