import Vue from 'vue';
import tagSelector from './vue/tagSelector';

new Vue({
    render: (h) => h(tagSelector)
}).$mount('#app');