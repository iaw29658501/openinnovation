import $ from "jquery";

$('.sendDeleteRequest').on('click', function () {
    const ideaId = $(this).data('ideaid');
    const token = $(this).data('token');
    $.post('/ideas/'+ideaId,
        {
            _token: token,
            _method: 'DELETE',
        }
    ).done(function (response) {
        console.log('delete state changed');
        console.log(response)
        //location.reload();
    }).fail(function () {
        console.log('error: like state not changed')
    });
});
$('.sendChangeLikeRequest').on('click', function (e) {
    // prevent default behaviour of browser to follow links
    e.preventDefault()
    const ideaId = $(this).data('ideaid');
    const token = $(this).data('token');
    $.get('/ideas/'+ideaId+'/change_like', { _token: token}
    ).done(function (response) {
        $('#likes-counter-'+ideaId)[0].innerHTML=response['count'];
    }).fail(function () {
        console.log('error: like state not changed')
    });
});
$('.sendDeleteCommentRequest').on('click', function () {
    const commentId = $(this).data('commentid');
    const token = $(this).data('token');
    $.post('/comment/'+commentId,
        {
            _token: token,
            _method: 'DELETE',
        }
    ).done(function () {
        console.log('deleted comment');
        console.log("lets remove " + commentId)
        $("#comment-"+commentId).remove()
    }).fail(function () {
        console.log('error: like state not changed')
    });
});