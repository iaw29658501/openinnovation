import $ from "jquery";
import 'dropzone'
$('.custom-file-input').change(function () {
    let files = [];
    let length = $(this)[0].files.length;
    if (length > 1) {
        let newHeight = length;
        $(this).siblings('label').animate({height: newHeight + 'em'}, 500);
        $(this).parent().animate({height: newHeight + 1 + 'em'}, 500)
    }
    for (let i = 0; i < length; i++) {
        files.push($(this)[0].files[i].name);
    }
    $(this).next('.custom-file-label').html(files.join(', '));
});

$('.file-input-col').on('click',
    '.delete-file', function (event) {
        $.ajax({
            type: "POST",
            url: $(this).data('url'),
            data: {
                _method: 'DELETE',
                _token: $(this).data('csrf')
            },
            success: function () {
                console.log('deleted file');
                event.currentTarget.parentElement.remove();
                this.remove();
            },
        });
    })