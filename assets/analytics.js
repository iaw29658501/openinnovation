import 'chart.js'
import $ from 'jquery';
import 'bootstrap-datepicker';
const textColor='#fff';
Chart.defaults.global.defaultFontColor = textColor;
document.addEventListener('DOMContentLoaded', function() {

function getDateData() {
    let dateData = document.querySelector('.date-data')
    return [ dateData.dataset.dateOne, dateData.dataset.dateTwo]
}
$.ajax({
    type: "POST",
    url: '/analytics/data/sector',
    data: { 'dates':getDateData() },
    success: function (response) {
        new Chart(document.getElementById("chartjs-ideas-per-sector"), {
            type: 'bar',
            data: response ,
            options: {
                legend: {
                    display: false,
                    position: 'bottom',
                    labels: {
                        fontColor: textColor
                    }
                }, scales: {
                    yAxes: [{
                        display: true,
                        ticks: {
                            beginAtZero: true
                        }
                    }],
                    xAxes:[{
                        ticks: {
                            fontSize: 16
                        }
                    }]
                }
            }
        });
    }
});
$.ajax({
    type: "POST",
    url: '/analytics/data/chart',
    data: { 'dates':getDateData() },
    success: function (data) {
        new Chart(document.getElementById("chartjs-graph"), {
            type: 'line',
            data: data,
            beginAtZero:true,
            options: {
                legend: {
                    labels: {
                        fontColor: textColor,
                        defaultFontColor: textColor
                    }
                }
            }
        });
    },
});

$.ajax({
    type: "POST",
    url: '/analytics/data/tags',
    data: { 'dates':getDateData() },
    success: function (response) {
        console.log(response)
        let isBar = response.labels.length > 8
        new Chart(document.getElementById("chartjs-tags-donut"), {
            type: isBar ? 'bar' : 'pie',
            data: response ,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: !isBar,
                    position: 'bottom',
                    labels: {
                        fontColor: textColor
                    }
                }
            }
        });
    },
});

$('.js-datepicker').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true
});
}, false);