import 'chart.js'
import $ from 'jquery';
import 'bootstrap-datepicker';
const textColor='#fff';
Chart.defaults.global.defaultFontColor = textColor;
$(window).on('load',function() {
    let idea_id=document.getElementById('js-idea-id').innerHTML
    $.ajax({
        type: "GET",
        url: `/analytics/data/idea/${idea_id}/notes_by_sector`,
        data: { },
        success: function (data) {
            new Chart(document.getElementById('js-chartjs-idea-notes-per-sector'), {
                type: 'bar',
                data: data,
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    legend: {
                        labels: {
                            fontColor: textColor,
                            defaultFontColor: textColor
                        }
                    }
                }
            });
        },
    });
})
