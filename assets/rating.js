import $ from 'jquery';
import 'jquery-bar-rating';

$(function() {
    $('.rating').barrating({
        theme: 'bars-reversed'
    });
});